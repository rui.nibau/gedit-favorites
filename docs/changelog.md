---
title:      Historique
date:       2014-04-27
updated:    2024-12-21
---

°°changelog°°
2024-12-21:
    • fix: [gedit 48.1 compatibility](https://framagit.org/rui.nibau/gedit-favorites/-/issues/4)
2024-05-01:
    • fix: [tepl 6 compatibility](https://gedit-technology.github.io/developer-docs/libgedit-tepl-6/api-breaks-during-tepl-6.html)
2023-12-03:
    • fix: Gedit 46 compatibility
    • upd: Back to date versionning (sorry)
45.0.0 (2023-09-29):
    • fix: Adaptation to Gedit 45.0
    • fix: (#2) Issues with unicode filename
2021-10-28:
    • upd: Simplification de l'extension
    • upd: Favoris stockés au format json
2020-05-01:
    • fix: Disparition de l'API ``Gedit.Document.get_location()``, remplacée par ``Gedit.Document.get_file().get_location()``.
2015-04-19:
    • upd: Première version diffusée.
2014-04-27:
    • add: Première version

