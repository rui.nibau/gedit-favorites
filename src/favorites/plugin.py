from gi.repository import Gedit, Gio, GObject, Tepl

from .favorites import Favorites

class FavoritesWindowActivatable(GObject.Object, Gedit.WindowActivatable):

    __gtype_name__ = 'FavoritesWindowActivatable'

    window = GObject.property(type=Gedit.Window)

    def __init__(self):
        GObject.Object.__init__(self)
        self._panel_item = None

    def do_activate(self):
        '''Activate window'''
        self.favorites = Favorites(self.window)
        self.favorites.connect('uri-activated', self.on_uri_activated)
        side_panel = self.window.get_side_panel()
        if isinstance(side_panel, Tepl.Panel):
            try:
                # for Gedit >= 48.1
                self._panel_item = Tepl.PanelItem.new(self.favorites.get_widget(), 'Favorites', 'Favorites', None, 0)
                side_panel.add(self._panel_item)
            except Exception:
                self._panel_item = None
                Tepl.Panel.add(side_panel, self.favorites.get_widget(), 'Favorites', 'Favorites')
        else:
            side_panel.add_titled(self.favorites.get_widget(), 'Favorites', 'Favorites')

    def do_deactivate(self):
        '''Deactivate window'''
        self.favorites.deactivate()
        if self._panel_item is None:
            self.window.get_side_panel().remove(self.favorites.get_widget())
        else:
            self.window.get_side_panel().remove(self._panel_item)
            self._panel_item = None

    def do_update_state(self):
        '''Upadate window state'''
        document = self.window.get_active_document()
        if document:
            location = document.get_file().get_location()
            if location:
                self.favorites.set_current_uri(location.get_uri())

    def on_uri_activated(self, favorites, uri):
        '''When user click on link'''
        location = Gio.file_new_for_uri(uri)
        tab = self.window.get_tab_from_location(location)
        if tab:
            self.window.set_active_tab(tab)
        else:
            tab = self.window.create_tab(True)
            tab.load_file(location, None, 0, 0, False)
