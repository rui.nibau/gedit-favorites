import os
import json
import urllib

from gi.repository import GLib, GObject, Gtk

FILE_DIR = os.path.dirname(__file__)
DATA_DIR = os.path.join(FILE_DIR, 'data')
GEDIT_CONFIG_DIR = os.path.join(GLib.get_user_config_dir(), 'gedit')
FAVORITES_FILE = os.path.join(GEDIT_CONFIG_DIR, 'favorites.json')

INDEX_NAME = 1
INDEX_URI = 2

ICON_FILE = 'text-x-generic-symbolic'

class Favorites(GObject.Object):

    __gtype_name__ = 'Favorites'

    '''
    @signal uri-activated (uri) when a file is activated
    '''
    __gsignals__ = {
        'uri-activated': (GObject.SIGNAL_RUN_FIRST, GObject.TYPE_NONE,
                            (GObject.TYPE_STRING,))
    }

    def __init__(self, window):
        GObject.Object.__init__(self)
        self.window = window

        # ui
        filename = os.path.join(DATA_DIR, 'ui', 'pane.glade')
        builder = Gtk.Builder()
        builder.add_objects_from_file(filename, ['box', 'store'])
        builder.connect_signals(self)
        self.__widget = builder.get_object('box')
        self.__tree = builder.get_object('tree')
        self.__store = builder.get_object('store')
        self.__cell = builder.get_object('cell_text')

        # data
        self.__current_uri = None
        if os.path.isfile(FAVORITES_FILE) is False:
            os.makedirs(GEDIT_CONFIG_DIR, exist_ok=True)
            with open(FAVORITES_FILE, 'w+', encoding='utf-8') as favfile:
                json.dump({}, favfile)
        self.__store.clear()
        with open(FAVORITES_FILE, 'r', encoding='utf-8') as favfile:
            data = favfile.read()
        favs = json.loads(data)
        for name, uri in favs.items():
            self.add_favorite(uri, name)
        self.__do_sort()

    def deactivate(self):
        self.save()

    def on_add_clicked(self, button):
        if self.__current_uri:
            treeiter = self.add_favorite(self.__current_uri)
            if treeiter:
                self.__set_focus_on_iter(treeiter, False)
                self.__do_sort()

    def on_remove_clicked(self, button):
        '''remove row'''
        treeiter = self.__get_selected_iter()
        if treeiter is not None:
            self.__store.remove(treeiter)
    
    def on_tree_row_activated(self, tree, path, column, data=None):
        uri = self.__get_uri_at_path(path)
        if uri:
            self.emit('uri-activated', uri)
    
    def on_cell_text_edited(self, renderer, path, text, data=None):
        self.__store[path][INDEX_NAME] = text
        self.__do_sort()

    def save(self):
        '''Save favorites file.'''
        favs = {}
        for row in self.__store:
            favs[row[1]] = row[2]
        with open(FAVORITES_FILE, 'w') as outfile:
            json.dump(favs, outfile)
    
    def is_in_store(self, uri):
        '''Is a uri already in favorites ?'''
        for row in self.__store:
            if row[INDEX_URI] == uri:
                return True
        return False
    
    def get_widget(self):
        return self.__widget
    
    def set_current_uri(self, uri):
        self.__current_uri = uri

    def add_favorite(self, uri, name=None):
        if not self.is_in_store(uri):
            if name is None:
                name = urllib.parse.unquote(os.path.basename(uri))
            treeiter = self.__store.append((ICON_FILE, name, uri))
            return treeiter
        return None

    def __do_sort(self):
        self.__store.set_sort_column_id(1, Gtk.SortType.ASCENDING)
    
    def __get_uri_at_iter(self, treeiter):
        return self.__store.get_value(treeiter, INDEX_URI)

    def __get_uri_at_path(self, path):
        treeiter = self.__store.get_iter(path)
        if treeiter:
            return self.__get_uri_at_iter(treeiter)
        return None

    def __get_selected_iter(self):
        selection = self.__tree.get_selection()
        model, treeiter = selection.get_selected()
        return treeiter

    def __set_focus_on_iter(self, treeiter, edit=True):
        path = self.__store.get_path(treeiter)
        self.__tree.get_selection().select_path(path)

